import { CustomerProfilePage } from './../pages/customer-profile/customer-profile';
import { MyOrdersCustomerPage } from './../pages/my-orders-customer/my-orders-customer';
import { ContentAdsPage } from './../pages/content-ads/content-ads';
import { MyListPage } from './../pages/my-list/my-list';
import { DesignCriteriaPage } from './../pages/design-criteria/design-criteria';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage, IonicStorageModule } from '@ionic/storage';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonTagsInputModule } from "ionic-tags-input";




import { MyApp } from './app.component';

import { MenuPage } from '../pages/menu/menu';
import { SignupPage } from '../pages/signup/signup';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { WelcomePage } from '../pages/welcome/welcome';
import { MainClientPage } from './../pages/main-client/main-client';
import { SignupInfluencerPage } from './../pages/signup-influencer/signup-influencer';
import { NewPasswordPage } from './../pages/new-password/new-password';
import { ForgetPasswordPage } from './../pages/forget-password/forget-password';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { VerificationPage } from '../pages/verification/verification';
import { ChooseCriteriaPage } from '../pages/choose-criteria/choose-criteria';
import { ChooseInfluencersPage } from '../pages/choose-influencers/choose-influencers';
import { InfluencersProfilePage } from '../pages/influencers-profile/influencers-profile';
import { ReviewsPage } from '../pages/reviews/reviews';
import { AfterVerificationPage } from '../pages/after-verification/after-verification';
import { ConfirmPage } from './../pages/confirm/confirm';
import { ChatListPage } from './../pages/chat-list/chat-list';
import { ChatPage } from './../pages/chat/chat';
import { ConfirmCampaignPage } from './../pages/confirm-campaign/confirm-campaign';
import { FilterPage } from './../pages/filter/filter';
import { InfluencerProfileEditPage } from './../pages/influencer-profile-edit/influencer-profile-edit';
import { ResultPage } from './../pages/result/result';
import { ContactusPage } from './../pages/contactus/contactus';



import { InfluencerOrdersPage } from './../pages/influencer-orders/influencer-orders';
import { ManageInfluencerOrdersPage } from './../pages/manage-influencer-orders/manage-influencer-orders';
import { ContentAdsCopyPage } from './../pages/content-ads-copy/content-ads-copy';
import { ManageCustomerOrdersPage } from './../pages/manage-customer-orders/manage-customer-orders';
import { TestingPage } from './../pages/testing/testing';
import { ReasonPopupPage } from './../pages/reason-popup/reason-popup';
import { ReasonRatingPage } from './../pages/reason-rating/reason-rating';
import { DetailInformationPage} from './../pages/detail-information/detail-information';
import { DeletePopupPage } from './../pages/delete-popup/delete-popup';
import { MessagePopupPage } from './../pages/message-popup/message-popup';
import { ReasonUnacceptPopupPage } from './../pages/reason-unaccept-popup/reason-unaccept-popup';

import { Api } from '../providers/api';
import { Items } from '../mocks/providers/items';
import { Settings } from '../providers/settings';
import { User } from '../providers/user';

import { Camera } from '@ionic-native/camera';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello',
    language: 'en'
  });
}


/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [
  MyApp,
  LoginPage,
  MenuPage,
  SettingsPage,
  SignupPage,
  TutorialPage,
  WelcomePage,
  ForgetPasswordPage,
  VerificationPage,
  NewPasswordPage,
  SignupInfluencerPage,
  MainClientPage,
  ChooseCriteriaPage,
  ChooseInfluencersPage,
  InfluencersProfilePage,
  ReviewsPage,
  AfterVerificationPage,
  ConfirmPage,
  DesignCriteriaPage,
  ChatListPage,
  ChatPage,
  MyListPage,
  ContentAdsPage,
  CustomerProfilePage,
  MyOrdersCustomerPage,
  ConfirmCampaignPage,
  FilterPage,
  InfluencerProfileEditPage,
  ManageInfluencerOrdersPage,
  ResultPage,
  ContactusPage,ReasonPopupPage,
  ReasonRatingPage,
  ContentAdsCopyPage,
  ManageCustomerOrdersPage,
  DetailInformationPage,
  TestingPage,
  DeletePopupPage,
  ReasonUnacceptPopupPage,
  MessagePopupPage
];

export function declarations() {
  return pages;
}

export function entryComponents() {
  return pages;
}

export function providers() {
  return [
    Api,
    Items,
    User,
    Camera,
    GoogleMaps,
    SplashScreen,
    StatusBar,

    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations(),
  imports: [
    BrowserModule,
    IonTagsInputModule,
    HttpModule,
    Ionic2RatingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      mode: 'ios'
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule { }
