import { MainClientPage } from './../pages/main-client/main-client';
import { Component, ViewChild } from '@angular/core';
import { Events, Platform, Nav, Config, MenuController } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ContentAdsCopyPage } from '../pages/content-ads-copy/content-ads-copy';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { Settings } from '../providers/providers';
import { ManageInfluencerOrdersPage } from '../pages/manage-influencer-orders/manage-influencer-orders';
import { ManageCustomerOrdersPage } from './../pages/manage-customer-orders/manage-customer-orders';
import { SignupInfluencerPage } from './../pages/signup-influencer/signup-influencer';
import { TestingPage } from './../pages/testing/testing';
import { DeletePopupPage } from './../pages/delete-popup/delete-popup';
import { MessagePopupPage } from './../pages/message-popup/message-popup';
import { ReasonUnacceptPopupPage } from './../pages/reason-unaccept-popup/reason-unaccept-popup';
import { TranslateService } from '@ngx-translate/core'
import { DesignCriteriaPage } from './../pages/design-criteria/design-criteria';

@Component({
  templateUrl: 'app.component.html'
})
export class MyApp {
    rootPage = ManageCustomerOrdersPage;
  menuSide: string;
  @ViewChild(Nav) nav: Nav;
  constructor(private translate: TranslateService, private platform: Platform, settings: Settings, private config: Config, statusBar: StatusBar, splashScreen: SplashScreen,
    public menuCtrl: MenuController, public events: Events) {

    this.initTranslate();

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    this.platform.setDir('ltr', true);

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });


  }
}
