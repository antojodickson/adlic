import { VerificationPage } from './../verification/verification';
import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';

import { User } from '../../providers/user';
import { LoginPage } from '../login/login';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, email: string, password: string } = {
    name: 'Test Human',
    email: 'test@example.com',
    password: 'test'
  };

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService, public loadingCtrl: LoadingController) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }
  
  isInfluencer : any;

  signUp() {
    let loader = this.loadingCtrl.create({
      content: 'Please Wait...'
    });

    loader.present().then(() => {
      console.log(this.isInfluencer);
      this.navCtrl.push(VerificationPage, {isInfluencer : this.isInfluencer});

      loader.dismiss();
    });
  }

  goToLogin() {
    this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
  }
}
