import { MyListPage } from './../my-list/my-list';
import { ReviewsPage } from './../reviews/reviews';
import { Component } from '@angular/core';
import { NavController, ModalController, MenuController, ActionSheetController } from 'ionic-angular';
import { Items } from '../../providers/providers';
import { Item } from '../../models/item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-customer-profile',
  templateUrl: 'customer-profile.html',
})
export class CustomerProfilePage {
  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, public translate: TranslateService, public menu: MenuController,
    public actionSheetCtrl: ActionSheetController) {
  }

  ionViewDidLoad() {
  }

  goToReviews() {
    this.navCtrl.push(ReviewsPage);
  }

}
