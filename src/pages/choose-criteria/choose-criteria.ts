import { Component, ViewChild } from '@angular/core';
import { NavController, ViewController} from 'ionic-angular';
import { Items } from '../../providers/providers';

import { Item } from '../../models/item';
@Component({
    selector: 'choose-criteria',
    templateUrl: 'choose-criteria.html'
})

export class ChooseCriteriaPage {
    currentItems: Item[];
    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public items: Items) {
        this.currentItems = this.items.query();
    }

    ionViewDidLoad() {

    }

    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }
}
