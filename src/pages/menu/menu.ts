import { MainClientPage } from './../main-client/main-client';
import { Component, ViewChild } from '@angular/core';
import { NavController, Nav, Platform, MenuController,Events } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})
export class MenuPage {
  // A reference to the ion-nav in our component
  profileImage: 'assets/img/avatar/girl-avatar.png';
  rootPage: any = LoginPage;

  pages: Array<{ title: string, component: any }>;

  clientPages: any[];
  influencerPages: any[];
  visitorPages: any[];
  activePageIndex: any = 1;
  currentUserType: any = 2; //1- visitor. 2- customer 3- influencer

  constructor(public navCtrl: NavController, private platform: Platform, public menuCtrl: MenuController,
    private translate: TranslateService, public events: Events) {
    this.clientPages = [
      { title: 'HOME', component: MainClientPage, icon: "ios-home-outline", index: 1 },
      { title: 'MYPROFILE', component: MainClientPage, icon: "ios-contact-outline", index: 2 },
      { title: 'MYORDER', component: MainClientPage, icon: "ios-basket-outline", index: 3 },
      { title: 'CHATS', component: MainClientPage, icon: "ios-chatbubbles-outline", index: 4 },
      { title: 'SETTINGS', component: MainClientPage, icon: "ios-options", index: 5 },
      { title: 'CONTACTUS', component: MainClientPage, icon: "ios-mail-outline", index: 6 },
      { title: 'LOGOUT', component: MainClientPage, icon: "ios-log-out-outline", index: 7 },
    ];

    this.influencerPages = [
      { title: 'HOME', component: MainClientPage, icon: "ios-home-outline", index: 8 },
      { title: 'MANAGESERVICES', component: MainClientPage, icon: "ios-contact-outline", index: 9 },
      { title: 'MYPROFILE', component: MainClientPage, icon: "ios-basket-outline", index: 10 },
      { title: 'VIEWREVIEWS', component: MainClientPage, icon: "ios-chatbubbles-outline", index: 11 },
      { title: 'CHATS', component: MainClientPage, icon: "ios-chatbubbles-outline", index: 12 },
      { title: 'LOGOUT', component: MainClientPage, icon: "ios-log-out-outline", index: 13 },
    ];

    this.visitorPages = [
      { title: 'HOME', component: MainClientPage, icon: "ios-home-outline", index: 14 },
    ];

    this.setClientPageTitles();
    this.setInfluencerPageTitles();
    this.setVisitorPageTitles();
    this.listenToLoginEvents();
  }

  setClientPageTitles() {
    for (let page of this.clientPages) {
      this.translate.get([page.title]).subscribe(values => {
        switch (page.title) {
          case 'HOME':
            page.title = values.HOME;
            break;
          case 'MYPROFILE':
            page.title = values.MYPROFILE;
            break;
          case 'MYORDER':
            page.title = values.MYORDER;
            break;
          case 'CHATS':
            page.title = values.CHATS;
            break;
          case 'SETTINGS':
            page.title = values.SETTINGS;
            break;
          case 'CONTACTUS':
            page.title = values.CONTACTUS;
            break;
          case 'LOGOUT':
            page.title = values.LOGOUT;
            break;
          case 'SWITCHTOINFLUENCER':
            page.title = values.SWITCHTOINFLUENCER;
            break;
        }
      });
    }
  }

  setInfluencerPageTitles() {
    for (let page of this.influencerPages) {
      this.translate.get([page.title]).subscribe(values => {
        switch (page.title) {
          case 'HOME':
            page.title = values.HOME;
            break;
          case 'MYPROFILE':
            page.title = values.MYPROFILE;
            break;
          case 'MANAGESERVICES':
            page.title = values.MANAGESERVICES;
            break;
          case 'CHATS':
            page.title = values.CHATS;
            break;
          case 'VIEWREVIEWS':
            page.title = values.VIEWREVIEWS;
            break;
          case 'LOGOUT':
            page.title = values.LOGOUT;
            break;
          case 'SWITCHTOCUSTOMER':
            page.title = values.SWITCHTOCUSTOMER;
            break;
        }
      });
    }
  }

  setVisitorPageTitles() {
    for (let page of this.visitorPages) {
      this.translate.get([page.title]).subscribe(values => {
        switch (page.title) {
          case 'HOME':
            page.title = values.HOME;
            break;
          case 'SWITCHTOCUSTOMER':
            page.title = values.SWITCHTOCUSTOMER;
            break;
        }
      });
    }
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(2);
    });

    this.events.subscribe('user:loginAsVisitor', () => {
      this.enableMenu(3);
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(1);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(1);
    });

    this.events.subscribe('user:switchToClient', () => {
      this.enableMenu(2);
    });

    this.events.subscribe('user:switchToInfluencer', () => {
      this.enableMenu(4);
    });
  }

  // Modes: 1- disable all menus. 2- enable client menu 3- enable visitor menu 4- enable influencer menu
  enableMenu(mode: any) {
    switch (mode) {
      case 1:
        this.menuCtrl.enable(false, 'viewsMenu');
        break;
      case 2:
        this.currentUserType = 2;
        this.activePageIndex = 1;
        break;
      case 3:
        this.currentUserType = 1;
        this.activePageIndex = 14;
        break;
      case 4:
        this.currentUserType = 3;
        this.activePageIndex = 8;
        break;
    }
  }

  openPage(page) {
    this.activePageIndex = page.index;
    switch (page.index) {
      case 7:
      case 13:
        this.events.publish('user:logout');
        this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        break;
      default:
        this.navCtrl.setRoot(page.component, {}, { animate: true, direction: 'forward' });
        break;
    }
  }

  switchAccount() {
    switch (this.currentUserType) {
      case 1:
        this.events.publish('user:logout');
        this.navCtrl.setRoot(SignupPage, {}, { animate: true, direction: 'forward' });
        break;
      case 2:
        this.menuCtrl.toggle("viewsMenu");
        this.events.publish('user:switchToClient');
        this.navCtrl.setRoot(MainClientPage, {}, { animate: true, direction: 'forward' });
        this.currentUserType = 3;
        this.activePageIndex = 8;
        break;
      case 3:

        this.menuCtrl.toggle("viewsMenu");
        this.events.publish('user:switchToInfluencer');
        this.navCtrl.setRoot(MainClientPage, {}, { animate: true, direction: 'forward' });
        this.currentUserType = 2;
        this.activePageIndex = 1;
        break;
    }
  }

  checkActive(pageIndex) {
    return false;
    //return pageIndex == this.activePageIndex;
  }

  ionViewDidLoad() {
    console.log('Hello MenuPage Page');
  }
}
