import { MainClientPage } from './../main-client/main-client'; 

import { AlertController, App, LoadingController, NavController, MenuController,Events } from 'ionic-angular';

import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'page-after-verification',
  templateUrl: 'after-verification.html',
})
export class AfterVerificationPage {

  constructor(private navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public app: App, public menu: MenuController,public events: Events) {
  }

  confirmNow() {
    this.navCtrl.setRoot(MainClientPage, {}, {animate: true, direction: 'forward'});
  } 

}
