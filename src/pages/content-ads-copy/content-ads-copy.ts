import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MenuPage } from './../menu/menu';
/**
 * Generated class for the ContentAdsCopyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-content-ads-copy',
  templateUrl: 'content-ads-copy.html',
})
export class ContentAdsCopyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  openMenu() {
      this.navCtrl.push(MenuPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ContentAdsCopyPage');
  }

}
