import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MainClientPage } from './../main-client/main-client';
import { ChatPage } from './../chat/chat';

@Component({
  selector: 'page-chat-list',
  templateUrl: 'chat-list.html',
})
export class ChatListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatListPage');
  }

  back(){
     this.navCtrl.pop();
  }

  goToChat()  {
    this.navCtrl.push(ChatPage);
    
  }


}
