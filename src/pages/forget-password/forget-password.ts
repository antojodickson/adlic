import { NewPasswordPage } from './../new-password/new-password';
import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';

import { User } from '../../providers/user';
import { LoginPage } from '../login/login';

import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html'
})
export class ForgetPasswordPage {

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService, public loadingCtrl: LoadingController) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  sendPassword() {
    let loader = this.loadingCtrl.create({
      content: 'Please Wait...'
    });

    loader.present().then(() => {
      this.navCtrl.setRoot(NewPasswordPage, {}, { animate: true, direction: 'forward' });
      loader.dismiss();
    });
  }

  goToLogin() {
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
  }
}
