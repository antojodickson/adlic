import { Component } from '@angular/core';
import { NavController, NavParams , ModalController ,MenuController} from 'ionic-angular';
import { Items } from '../../providers/providers';
import { Item } from '../../models/item'; 
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-reviews',
  templateUrl: 'reviews.html',
})
export class ReviewsPage {

currentItems: Item[];
      textDir: string;
      ionItemDir: string;
      ionItemSide: string;
constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, translate: TranslateService, public menu: MenuController) {
    this.currentItems = this.items.query();
    this.textDir = translate.currentLang == 'ar' ? 'rtl' : 'ltr';
    this.ionItemDir = translate.currentLang == 'ar' ? 'ltr' : 'rtl';
    this.ionItemSide = translate.currentLang == 'ar' ? 'right' : 'left';
  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewsPage');
  }

}
