import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, MenuController,AlertController  } from 'ionic-angular';
import { Items } from '../../providers/providers';
import { Item } from '../../models/item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'my-list',
  templateUrl: 'my-list.html',
})
export class MyListPage {

  currentItems: Item[];
  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, translate: TranslateService, public menu: MenuController,
  public alertCtrl: AlertController) {
    this.currentItems = this.items.getMyList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewsPage');
  }

  deleteItem()
  {
    let confirm = this.alertCtrl.create({
      title: 'Delete Service',
      message: 'Are you sure you want to delete service 2?',
      buttons: [
        {
          text: 'Back',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

}
