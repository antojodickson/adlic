import { MainClientPage } from './../main-client/main-client';
import { AlertController, App, LoadingController, NavController, MenuController, Events } from 'ionic-angular';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { AfterVerificationPage } from './../after-verification/after-verification';
import { SignupInfluencerPage } from './../signup-influencer/signup-influencer';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html',
})
export class VerificationPage {
  pleaseWaitText: "";
  public resendColor;
  isOutline;
  constructor(private navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController
    , public app: App, public menu: MenuController, public events: Events,public translate: TranslateService) {
      this.translate.get('PLEASEWAIT').subscribe(
      value => {
        this.pleaseWaitText = value;
      });
      this.resendColor ="dark";
      this.isOutline=true;
  }

  ionViewDidLoad() {

  }

  resendCode() {
    let loader = this.loadingCtrl.create({
      content: this.pleaseWaitText
    });

    loader.present().then(() => {
      this.resendColor = "AuthButton";
      this.isOutline =false;
      loader.dismiss();
    });
  }


  VerifyCode() {
    let loader = this.loadingCtrl.create({
      content: this.pleaseWaitText
    });

    loader.present().then(() => {
      this.navCtrl.push(SignupInfluencerPage, {}, { animate: true, direction: 'forward' });
      loader.dismiss();
    });
    
  }





}
