import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';

import { LoginPage } from '../login/login';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-new-password',
  templateUrl: 'new-password.html'
})  
export class NewPasswordPage {

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public translateService: TranslateService, public loadingCtrl: LoadingController) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  resetPassword() {
    let loader = this.loadingCtrl.create({
      content: 'Please Wait...'
    });

    loader.present().then(() => {
      this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
      loader.dismiss();
    });
  }
}
