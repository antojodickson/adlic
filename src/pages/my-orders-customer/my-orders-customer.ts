import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, MenuController,AlertController  } from 'ionic-angular';
import { Items } from '../../providers/providers';
import { Item } from '../../models/item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-my-orders-customer',
  templateUrl: 'my-orders-customer.html',
})
export class MyOrdersCustomerPage {

  currentItems: Item[];
  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, translate: TranslateService, public menu: MenuController,
  public alertCtrl: AlertController) {
    this.currentItems = this.items.getMyList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewsPage');
  }

}
