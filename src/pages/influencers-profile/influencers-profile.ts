import { MyListPage } from './../my-list/my-list';
import { ReviewsPage } from './../reviews/reviews';
import { Component } from '@angular/core';
import { NavController, ModalController, MenuController, ActionSheetController } from 'ionic-angular';
import { Items } from '../../providers/providers';
import { Item } from '../../models/item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-influencers-profile',
  templateUrl: 'influencers-profile.html',
})
export class InfluencersProfilePage {


  imageUrl: string = 'https://s-media-cache-ak0.pinimg.com/736x/7f/79/6d/7f796d57218d9cd81a92d9e6e8e51ce4.jpg';
  currentItems: Item[];
  textDir: string;
  ionItemDir: string;
  ionItemSide: string;

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, public translate: TranslateService, public menu: MenuController,
    public actionSheetCtrl: ActionSheetController) {
    this.currentItems = this.items.query();
    this.textDir = translate.currentLang == 'ar' ? 'rtl' : 'ltr';
    this.ionItemDir = translate.currentLang == 'ar' ? 'ltr' : 'rtl';
    this.ionItemSide = translate.currentLang == 'ar' ? 'right' : 'left';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfluencersProfilePage');
  }

  goToReviews() {
    this.navCtrl.push(ReviewsPage);
  }

  showConfirm() {
    var alertTitle;
    var goToListTitle;
    var continuesToAddTitle;

    this.translate.get('ADDEDSUCCESSFULLY').subscribe(
      value => {
        alertTitle = value;
      }
    );
    this.translate.get('GOTOLIST').subscribe(
      value => {
        goToListTitle = value;
      }
    );
    this.translate.get('CONTINUESTOADD').subscribe(
      value => {
        continuesToAddTitle = value;
      }
    );
    let actionSheet = this.actionSheetCtrl.create({
      title: alertTitle,
      buttons: [
        {
          text: goToListTitle,
          handler: () => {
            this.navCtrl.push(MyListPage);
          }
        }, {
          text: continuesToAddTitle,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

}
