import { MainClientPage } from './../main-client/main-client';
import { VerificationPage } from './../verification/verification';
import { AfterVerificationPage } from './../after-verification/after-verification';
import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController,Events } from 'ionic-angular';

import { User } from '../../providers/user';
import { LoginPage } from '../login/login';

import { TranslateService } from '@ngx-translate/core';
import { IonTagsInput } from "./ion-tags-input";

@Component({
  selector: 'page-signup-influencer',
  templateUrl: 'signup-influencer.html'
})
export class SignupInfluencerPage {

  // Our translated text strings
  private signupErrorString: string;

  tags = [];
  add:boolean = false;
  newTag:string;
  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService, public loadingCtrl: LoadingController,public events: Events) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  onChange(val){

  }


  signUpAsInfluencer() {
    let loader = this.loadingCtrl.create({
      content: 'Please Wait...'
    });

    loader.present().then(() => {
      this.events.publish('user:switchToClient');
      this.navCtrl.setRoot(MainClientPage, {}, {animate: true, direction: 'forward'});
      loader.dismiss();
    });
  }

  goToLogin() {
    this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
  }


  goToVerfication(){
    this.navCtrl.setRoot(AfterVerificationPage, {}, {animate: true, direction: 'forward'});
  }

  addTag(event) {
    if(event.keyCode == '13') {
      this.tags.push(this.newTag);
      this.add = false;
      this.newTag = '';
    }
  }

  enableInput() {
    this.add = true;
    setTimeout(() => {
      document.getElementById('tag-input').focus();
    }, 500);
  }

  removeTag(index) {
      this.tags.splice(index,1);
  }
}
