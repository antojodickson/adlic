import { VerificationPage } from './../verification/verification';
import { MainClientPage } from './../main-client/main-client';
import { ForgetPasswordPage } from './../forget-password/forget-password';
import { MyOrdersCustomerPage } from './../my-orders-customer/my-orders-customer';
import { ReasonPopupPage } from './../reason-popup/reason-popup';
import { ReasonRatingPage } from './../reason-rating/reason-rating';
import { SignupPage } from './../signup/signup';
import { TranslateService } from '@ngx-translate/core';
import { AlertController, App, LoadingController, NavController, MenuController,Events } from 'ionic-angular';
// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  AuthTab: string = "loginTab";
  public loginForm: any;
  pleaseWaitText: "";
  constructor(private navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController
    , public app: App, public menu: MenuController,public events: Events,public translate: TranslateService) {
    this.translate.get('PLEASEWAIT').subscribe(
      value => {
        this.pleaseWaitText = value;
      });
  }

  login() {
    let loader = this.loadingCtrl.create({
      content: this.pleaseWaitText
    });

    loader.present().then(() => {
      this.events.publish('user:login');
      this.navCtrl.setRoot(MainClientPage, {}, {animate: true, direction: 'forward'});
      loader.dismiss();
    });
  }

  goToPage() {
    this.navCtrl.push(ReasonRatingPage);
  }

  loginAsVisitor() {
    let loader = this.loadingCtrl.create({
      content: this.pleaseWaitText
    });

    loader.present().then(() => {
      this.events.publish('user:loginAsVisitor');
      this.navCtrl.setRoot(MainClientPage, {}, {animate: true, direction: 'forward'});
      loader.dismiss();
    });
  }

  goToSignup() {
    let loader = this.loadingCtrl.create({
      content: this.pleaseWaitText
    });

    loader.present().then(() => {
      this.navCtrl.push(VerificationPage, {}, {animate: true, direction: 'forward'});
      loader.dismiss();
    });
  }


  goToForgetPassword() {
    this.navCtrl.setRoot(ForgetPasswordPage, {}, {animate: true, direction: 'forward'});
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

}
