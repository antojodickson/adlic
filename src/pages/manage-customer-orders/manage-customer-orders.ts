import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MenuPage } from './../menu/menu';

/**
 * Generated class for the ManageCustomerOrdersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-manage-customer-orders',
  templateUrl: 'manage-customer-orders.html',
})
export class ManageCustomerOrdersPage {
selectedTab:string = 'manage';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openMenu() {
      this.navCtrl.push(MenuPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManageCustomerOrdersPage');
  }

}
