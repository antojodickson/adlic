import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MenuPage } from './../menu/menu';
import { ChooseCriteriaPage } from './../choose-criteria/choose-criteria';
/**
 * Generated class for the ManageInfluencerOrdersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-manage-influencer-orders',
  templateUrl: 'manage-influencer-orders.html',
})
export class ManageInfluencerOrdersPage {
  selectedTab:string = 'manage';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
      this.navCtrl.push(ChooseCriteriaPage);
  }

  openMenu() {
      this.navCtrl.push(MenuPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ManageInfluencerOrdersPage');
  }

}
