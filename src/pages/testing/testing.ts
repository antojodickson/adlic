import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { ReasonPopupPage } from '../reason-popup/reason-popup';
import { ReasonRatingPage } from '../reason-rating/reason-rating';
import { DetailInformationPage} from '../detail-information/detail-information';
import { DeletePopupPage } from '../delete-popup/delete-popup';
import { MessagePopupPage } from '../message-popup/message-popup';
import { ReasonUnacceptPopupPage } from '../reason-unaccept-popup/reason-unaccept-popup';
/**
 * Generated class for the TestingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-testing',
  templateUrl: 'testing.html',
})
export class TestingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  openDeletePopup() {
    let DeletePopup = this.modalCtrl.create(DeletePopupPage);
    DeletePopup.present();
  }

  openReasonPopup() {
    let ReasonPopup = this.modalCtrl.create(ReasonPopupPage);
    ReasonPopup.present();
  }
  openRatingPopup() {
    let RatingPopup = this.modalCtrl.create(ReasonRatingPage);
    RatingPopup.present();
  }

  openMessagePopup() {
    let MessagePopup = this.modalCtrl.create(MessagePopupPage);
    MessagePopup.present();
  }

  openDetailInformation() {
    let DetailInformation = this.modalCtrl.create(DetailInformationPage);
    DetailInformation.present();
  }
  openReasonUnacceptPopup() {
    let ReasonUnacceptPopup = this.modalCtrl.create(ReasonUnacceptPopupPage);
    ReasonUnacceptPopup.present();
  }
  // openReason() {
  //   let reason = this.modalCtrl.create(ReasonPopupPage);
  //   reason.present();
  // }
  // openReason() {
  //   let reason = this.modalCtrl.create(ReasonPopupPage);
  //   reason.present();
  // }
  // openReason() {
  //   let reason = this.modalCtrl.create(ReasonPopupPage);
  //   reason.present();
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestingPage');
  }

}
