import { Component, ViewChild } from '@angular/core';
import { NavController, ToastController, ViewController} from 'ionic-angular';
import { Items } from '../../providers/providers';

import { IonTagsInput } from "./ion-tags-input";

import { Item } from '../../models/item';
@Component({
    selector: 'design-criteria',
    templateUrl: 'design-criteria.html'
})

export class DesignCriteriaPage {
    currentItems: Item[];
    tags = [];
    add:boolean = false;
    newTag:string;
    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController, public items: Items) {
        this.currentItems = this.items.query();
    }

    onChange(val){

    }

    ionViewDidLoad() {

    }

    addTag(event) {
      if(event.keyCode == '13') {
        this.tags.push(this.newTag);
        this.add = false;
        this.newTag = '';
      }
    }

    enableInput() {
      this.add = true;
      setTimeout(() => {
        document.getElementById('tag-input').focus();
      }, 500);
    }

    removeTag(index) {
        this.tags.splice(index,1);
    }

    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }
}
