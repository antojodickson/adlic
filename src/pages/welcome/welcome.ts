import { AlertController, App, LoadingController, NavController, MenuController } from 'ionic-angular';
// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component } from '@angular/core';
/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  public loginForm: any;

  constructor(private navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public app: App, public menu: MenuController) {
    
  }

  login() {
    let loader = this.loadingCtrl.create({
      content: 'Please Wait...'
    });

    loader.present().then(() => {
      //this.navCtrl.push(TabsPage);
      loader.dismiss();
    });
  }

  loginAsVisitor() {
    let loader = this.loadingCtrl.create({
      content: 'Please Wait...'
    });

    loader.present().then(() => {
      //this.navCtrl.setRoot(TabsPage);
      loader.dismiss();
    });
  }

  signUp() {
    let loader = this.loadingCtrl.create({
      content: 'Please Wait...'
    });

    loader.present().then(() => {
      //this.navCtrl.setRoot(SignupPage);
      loader.dismiss();
    });
  }

  goToSignup() {
    //this.navCtrl.setRoot(SignupPage);
  }

  goToForgetPassword() {
    //this.navCtrl.setRoot(ForgetPasswordPage);
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

}
