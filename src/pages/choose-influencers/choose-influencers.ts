import { InfluencersProfilePage } from './../influencers-profile/influencers-profile';
import { Component } from '@angular/core';
import { NavController, ModalController ,MenuController} from 'ionic-angular';

import { ItemDetailPage } from '../item-detail/item-detail';
import { Items } from '../../providers/providers';
import { ChooseCriteriaPage } from '../../pages/choose-criteria/choose-criteria';
import { Item } from '../../models/item';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-choose-influencers',
  templateUrl: 'choose-influencers.html',
})
export class ChooseInfluencersPage {

      currentItems: Item[];
      textDir: string;
      ionItemDir: string;
      ionItemSide: string;
constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, translate: TranslateService, public menu: MenuController) {
    this.currentItems = this.items.query();
    this.textDir = translate.currentLang == 'ar' ? 'rtl' : 'ltr';
    this.ionItemDir = translate.currentLang == 'ar' ? 'ltr' : 'rtl';
    this.ionItemSide = translate.currentLang == 'ar' ? 'right' : 'left';
  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChooseInfluencersPage');
  }


  addItem() {
   
  }


  openItem(){
    this.navCtrl.push(InfluencersProfilePage);
  }

}
