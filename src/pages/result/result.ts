import { Component,ViewChild  } from '@angular/core';
import { NavController, NavParams,Slides ,Platform, AlertController, ModalController  } from 'ionic-angular';

import { ReasonRatingPage } from '../reason-rating/reason-rating';
import { ReasonPopupPage } from '../reason-popup/reason-popup';
/**
 * Generated class for the ResultPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {
  @ViewChild(Slides) slides: Slides;
  dirLang: any;


  goToSlide() {
    this.slides.slideTo(2, 500);
  }


  constructor(public navCtrl: NavController, public navParams: NavParams,private platform: Platform, public alertCtrl: AlertController, public modalCtrl: ModalController) {
    if (this.platform.isRTL) {
      this.dirLang = "rtl";
    }
    else{
        this.dirLang = "ltr";
      }

  }

satisfied() {
   let modal = this.modalCtrl.create(ReasonRatingPage);
    modal.present();
 }
complain() {
   let modal = this.modalCtrl.create(ReasonPopupPage);
    modal.present();
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPage');
  }

}
