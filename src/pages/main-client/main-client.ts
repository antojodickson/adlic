import { InfluencersProfilePage } from './../influencers-profile/influencers-profile';
import { DesignCriteriaPage } from './../design-criteria/design-criteria';
import { MenuPage } from './../menu/menu';
import { ChooseCriteriaPage } from './../choose-criteria/choose-criteria';
import { Component } from '@angular/core';
import { NavController, ModalController, MenuController } from 'ionic-angular';

import { Items } from '../../providers/providers';

import { Item } from '../../models/item';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'page-main-client',
    templateUrl: 'main-client.html'
})
export class MainClientPage {
    currentItems: Item[];
    textDir: string;
    ionItemDir: string;
    ionItemSide: string;
    constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, public translate: TranslateService, public menu: MenuController) {
        this.currentItems = this.items.query();
        this.textDir = translate.currentLang == 'ar' ? 'rtl' : 'ltr';
        this.ionItemDir = translate.currentLang == 'ar' ? 'ltr' : 'rtl';
        this.ionItemSide = translate.currentLang == 'ar' ? 'right' : 'left';
    }

    /**
     * The view loaded, let's query our items for the list
     */
    ionViewDidLoad() {
    }

    /**
     * Prompt the user to add a new item. This shows our ItemCreatePage in a
     * modal and then adds the new item to our data source if the user created one.
     */
    addItem() {
        this.navCtrl.push(ChooseCriteriaPage);
    }

    openMenu() {
        this.navCtrl.push(MenuPage);
    }

    /**
     * Delete an item from the list of items.
     */
    deleteItem(item) {
        this.items.delete(item);
    }

    /**
     * Navigate to the detail page for this item.
     */
    openItem(item: Item) {
        this.navCtrl.push(ChooseCriteriaPage, {
            item: item
        });
    }

    openDesignCriteria() {
        this.navCtrl.push(DesignCriteriaPage);
    }

    ionViewDidEnter() {
        this.menu.enable(true);
    }

    openInfluencerProfile()
    {
        this.navCtrl.push(InfluencersProfilePage);
    }
}
